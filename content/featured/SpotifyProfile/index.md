---
date: '1'
title: 'VTip'
cover: './vtip.jpeg'
external: 'https://apps.apple.com/in/app/vtip/id1478514502'
tech:
  - Flutter
  - Node.js
  - Express
showInProjects: true
---
It's a social activity enhancing and notification iOS app. Especially build for small communities. Both App Admins and App Beneficiaries can make use of this app.