---
date: '2'
title: 'CCC BIHER'
cover: './ccc.jpeg'
external: 'http://cccbiher.co.in'
tech:
  - JQuery
  - Angular JS
  - Nodejs
  - Express
  - Mongo DB
showInProjects: true
---

This is a Nodejs web app built for my club. This will enables to track the club devs activity and their bio.