---
date: '2018-09-10'
title: 'Tech Lead'
company: 'Creative Computing'
location: 'Chennai'
range: 'September 2018 - December 2018'
url: 'http://cccbiher.co.in'
---

- Established this Tech club and reached students to develop and enrich their technical skills. 
- Behalf of the club I hosted a workshop with 250+ attendies.
- Being a Tech Lead, Many technical competitions and campaigns are hosted by me
